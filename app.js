//importing modules
const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const taskRoute = require('./routes/taskRoutes')


//initialize dotenv
dotenv.config()

//server setup
const app = express()
const port = 3003

app.use(express.json())
app.use(express.urlencoded({extended: true}))

// mongodb connection

mongoose.connect(`mongodb+srv://vonarceo:${process.env.PASSWORD}@cluster0.x7hwzcj.mongodb.net/S36-todo?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})


let db = mongoose.connection

db.on('error', () => console.error('Connection Error.'))
db.on('open', () => console.log('We are connected to mongoDB!'))

//routes
app.use('/tasks', taskRoute)



//server listening
app.listen(port, () => console.log(`Server running in localhost:${port}`))


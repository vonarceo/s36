const Task = require('../models/Task.js')

module.exports.createTask = (data) => {
	let newTask = new Task({
		name: data.name
	})
//.then 1st parameter = result, 2nd = not found
	return newTask.save().then((savedTask, error) => {
		if(error) {
			console.log(error)
			return error
		}

		return savedTask
	})

}
//means get all task collection
module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result
	})
}


//new func

module.exports.updateTask = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) =>{
		if(error) {
			console.log(error)
			return error
		}

		result.name = new_data.name

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return error
			}
			return updatedTask
		})
	})
}

module.exports.deleteTask = (task_id, data) => {
	return Task.findByIdAndDelete(task_id).then((result, error) =>{
		if(error) {
			console.log(error)
			return error
		}

		else {
			return result
		}
	})
}

module.exports.getSpecificTasks = (task_id) => {
	return Task.find({_id: task_id}).then((result) => {
		return result
	})
}


module.exports.updateStatus = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) =>{
		if(error) {
			console.log(error)
			return error
		}

		result.status = new_data.status

		return result.save().then((updatedStatus, error) => {
			if(error){
				console.log(error)
				return error
			}
			return updatedStatus
		})
	})
}
const express = require('express')
const router = express.Router()
const TaskController = require('../controllers/TaskController')

// create single task

router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((result) => {
		response.send(result)
	})
})

//get all tasks
router.get('/',(request, response) => {
	TaskController.getAllTasks().then((result) => {
		response.send(result)
	})
})



//new tasks

router.patch('/:id/update', (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})


router.delete('/:id/delete', (request, response) => {
	TaskController.deleteTask(request.params.id, request.body).then((result) => {
		response.send(`Successfully Deleted the task with the name ${request.body.name}`)
	})
})



router.get('/:id',(request, response) => {
	TaskController.getSpecificTasks(request.params.id).then((result) => {
		response.send(result)
	})
})

router.patch('/:id/complete', (request, response) => {
	TaskController.updateStatus(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})




module.exports = router

